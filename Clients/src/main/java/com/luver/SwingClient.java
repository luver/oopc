package com.luver;

import com.luver.swingClient.models.PolynomJTableModel;
import com.luver.swingClient.models.TableJTableModel;
import com.luver.swingClient.models.TaskJTableModel;
import com.luver.xml.XMLUtil;
import net.miginfocom.swing.MigLayout;
import org.jfree.chart.ChartPanel;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class SwingClient implements Client {
    private String fileName;
    private JFrame mainFrame = new JFrame();
    private JPanel mainPanel = new JPanel();
    private JTable polynomTable = new JTable(new PolynomJTableModel());
    private JTable tableTable = new JTable(new TableJTableModel());
    private JMenuBar menuBar = new JMenuBar();
    private ChartPanel chartPanel;
    private JTable taskTable = new JTable(new TaskJTableModel());
    private JPanel solvingPanel = new JPanel(new MigLayout());
    private JLabel resultLabel = new JLabel("Корiнь: ");
    private ReportBuilder reportBuilder = new ReportBuilder();
    private Double result;

    @Override
    public void run() {
        tableTable.getModel().addTableModelListener(new TableModelListener() {
            @Override
            public void tableChanged(TableModelEvent e) {
                tableTable.updateUI();
            }
        });
        setMenu();
        mainFrame.add(mainPanel);
        mainPanel.setLayout(new MigLayout());
        // Polynom
        JPanel polynomPanel = new JPanel(new MigLayout());
        polynomPanel.setMaximumSize(new Dimension(10000, 120));
        polynomPanel.setPreferredSize(new Dimension(250, 120));
        JScrollPane polynomScrollPane = new JScrollPane(polynomTable);
        polynomScrollPane.setMaximumSize(new Dimension(10000, 100));
        polynomPanel.add(polynomScrollPane, "cell 0 0");
        polynomPanel.setBorder(BorderFactory.createTitledBorder("Полiном"));
        mainPanel.add(polynomPanel);
        // Table
        JPanel tablePanel = new JPanel(new MigLayout());
        tablePanel.setMaximumSize(new Dimension(10000, 120));
        tablePanel.setPreferredSize(new Dimension(250, 120));
        JScrollPane tableScrollPane = new JScrollPane(tableTable);
        tableScrollPane.setMaximumSize(new Dimension(1000, 100));
        tablePanel.add(tableScrollPane, "cell 0 1");
        tablePanel.setBorder(BorderFactory.createTitledBorder("Таблиця"));
        mainPanel.add(tablePanel);
        // Task
        JPanel taskPanel = new JPanel(new MigLayout());
        taskPanel.setMaximumSize(new Dimension(10000, 120));
        taskPanel.setPreferredSize(new Dimension(200, 120));
        taskPanel.add(taskTable, "cell 0 2");
        taskTable.setMaximumSize(new Dimension(10000, 100));
        taskPanel.setBorder(BorderFactory.createTitledBorder("Умови"));
        mainPanel.add(taskPanel, "wrap");
        // Solving

        solvingPanel.setBorder(BorderFactory.createTitledBorder("Рiшення"));
        solvingPanel.setMinimumSize(new Dimension(540, 540));
        solvingPanel.setPreferredSize(new Dimension(540, 540));
        mainPanel.add(solvingPanel, "span 2");
        mainPanel.add(resultLabel);
        mainFrame.setJMenuBar(menuBar);
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.pack();
        mainFrame.setVisible(true);
    }

    private void setMenu() {
        JMenu fileMenu = new JMenu("Файл");
        menuBar.add(fileMenu);
        JMenuItem item = new JMenuItem("Вiдкрити");
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                JFileChooser fc = new JFileChooser();
                fc.addChoosableFileFilter(new FileNameExtensionFilter("XML files", "xml"));
                int val = fc.showOpenDialog(mainPanel);
                File file = fc.getSelectedFile();
                if (val == JFileChooser.APPROVE_OPTION) {
                    fileName = file.getAbsolutePath();
                    polynomTable.setModel(new PolynomJTableModel(XMLUtil.readPolynomial(file.getAbsolutePath())));
                    tableTable.setModel(new TableJTableModel(XMLUtil.readTable(file.getAbsolutePath())));
                }
            }
        });
        fileMenu.add(item);
        item = new JMenuItem("Зберегти");
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {

                if (fileName != null) {
                    XMLUtil.save(fileName, ((TableJTableModel) tableTable.getModel()).getTable(),
                            ((PolynomJTableModel) polynomTable.getModel()).getPolynomial());
                } else {
                    JFileChooser fc = new JFileChooser();
                    int val = fc.showSaveDialog(mainPanel);
                    if (val == JFileChooser.APPROVE_OPTION) {
                        File file = fc.getSelectedFile();
                        XMLUtil.save(file.getAbsolutePath(), ((TableJTableModel) tableTable.getModel()).getTable(),
                                ((PolynomJTableModel) polynomTable.getModel()).getPolynomial());
                    }
                }
            }
        });
        fileMenu.add(item);
        item = new JMenuItem("Зберегти як");
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                JFileChooser fc = new JFileChooser();

                int val = fc.showSaveDialog(mainPanel);
                if (val == JFileChooser.APPROVE_OPTION) {
                    File file = fc.getSelectedFile();
                    XMLUtil.save(file.getAbsolutePath(), ((TableJTableModel) tableTable.getModel()).getTable(),
                            ((PolynomJTableModel) polynomTable.getModel()).getPolynomial());
                    fileName = file.getAbsolutePath();
                }
            }
        });
        fileMenu.add(item);

        item = new JMenuItem("Зберегти звiт");
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                JFileChooser fc = new JFileChooser();
                fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                int val = fc.showSaveDialog(mainPanel);
                if (val == JFileChooser.APPROVE_OPTION) {
                    File file = fc.getSelectedFile();
                    try {
                        reportBuilder.setResult(result);
                        reportBuilder.createReport().saveToFile(file.getAbsolutePath());
                    } catch (IOException e) {

                    }
                }
            }
        });
        fileMenu.add(item);

        item = new JMenuItem("Справка");
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                try {
                    File file = new File("help/contents.html");
                    Desktop.getDesktop().open(file);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        fileMenu.add(item);

        JMenu solvingMenu = new JMenu("Знайти коренi");
        item = new JMenuItem("Лагранжа iнтеполяцiя");
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                solveLagrange();
            }
        });

        solvingMenu.add(item);
        item = new JMenuItem("Лiнiйна iнтеполяцiя");
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                solveLinear();
            }
        });
        solvingMenu.add(item);
        item = new JMenuItem("Додати точку");
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ((TableJTableModel) tableTable.getModel()).addPoint();
                tableTable.updateUI();
            }
        });
        solvingMenu.add(item);
        JMenuItem ssitem = new JMenuItem("Видалити точку");
        ssitem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ((TableJTableModel) tableTable.getModel()).deletePoint();
                tableTable.updateUI();
            }
        });
        solvingMenu.add(ssitem);
        JMenuItem sitem = new JMenuItem("Додати коефіцієнт");
        sitem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ((PolynomJTableModel) polynomTable.getModel()).addCoef();
                polynomTable.updateUI();
            }
        });
        solvingMenu.add(sitem);

        sitem = new JMenuItem("Видалити коефіцієнт");
        sitem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                ((PolynomJTableModel) polynomTable.getModel()).deleteCoef();
                polynomTable.updateUI();
            }
        });
        solvingMenu.add(sitem);

        menuBar.add(solvingMenu);
    }

    private void solveLagrange() {
        double left = Double.parseDouble(taskTable.getValueAt(0, 1).toString());
        double right = Double.parseDouble(taskTable.getValueAt(1, 1).toString());
        double error = Double.parseDouble(taskTable.getValueAt(2, 1).toString());
        reportBuilder.setConstraints(left, right, error);
        reportBuilder.setPolynomial(((PolynomJTableModel) polynomTable.getModel()).getPolynomial());
        reportBuilder.setTable(((TableJTableModel) tableTable.getModel()).getTable());

        Solver solver = new DichotomySolver();
        Function interpolation = new LagrangeInterpolation(((TableJTableModel) tableTable.getModel()).getTable());
        reportBuilder.setInterpolation(interpolation);
        Equation equation = new Equation(((PolynomJTableModel) polynomTable.getModel()).getPolynomial(), interpolation);
        try {
            if (error <= 0.0) throw new RuntimeException("Невiрна похибка");
            result = solver.solve(equation, left, right, error);
            resultLabel.setText("Корiнь: " + result.toString());
        } catch (Exception e) {
            resultLabel.setText("Корiнь: " + e.getMessage());
        } finally {
            if (chartPanel != null) solvingPanel.remove(0);
            chartPanel = new ChartPanel(reportBuilder.createReport().getChart());
            solvingPanel.add(chartPanel, "cell 0 0");
            chartPanel.setMinimumSize(new Dimension(500, 500));
            chartPanel.setPreferredSize(new Dimension(500, 500));
            solvingPanel.updateUI();
        }


    }

    private void solveLinear() {
        double left = Double.parseDouble(taskTable.getValueAt(0, 1).toString());
        double right = Double.parseDouble(taskTable.getValueAt(1, 1).toString());
        double error = Double.parseDouble(taskTable.getValueAt(2, 1).toString());
        reportBuilder.setConstraints(left, right, error);
        reportBuilder.setPolynomial(((PolynomJTableModel) polynomTable.getModel()).getPolynomial());
        reportBuilder.setTable(((TableJTableModel) tableTable.getModel()).getTable());
        Solver solver = new DichotomySolver();
        Function interpolation = new LinearInterpolation(((TableJTableModel) tableTable.getModel()).getTable());
        reportBuilder.setInterpolation(interpolation);
        Equation equation = new Equation(((PolynomJTableModel) polynomTable.getModel()).getPolynomial(), interpolation);
        try {
            if (error <= 0.0) throw new RuntimeException("Невiрна похибка");
            result = solver.solve(equation, left, right, error);

            if (chartPanel != null) solvingPanel.remove(0);
            chartPanel = new ChartPanel(reportBuilder.createReport().getChart());
            solvingPanel.add(chartPanel, "cell 0 0");
            chartPanel.setMinimumSize(new Dimension(500, 500));
            chartPanel.setPreferredSize(new Dimension(500, 500));
            solvingPanel.updateUI();
            // solvingPanel.repaint();
            resultLabel.setText("Корiнь: " + result.toString());
        } catch (Exception e) {
            resultLabel.setText("Корiнь: " + e.getMessage());
        }
    }
}
