package com.luver.xml;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

public class Record implements java.io.Serializable {
    private com.luver.polynomial.castor.Polynomial _polynomial;

    private com.luver.table.castor.Table _table;


    public Record() {
        super();
    }

    public com.luver.polynomial.castor.Polynomial getPolynomial() {
        return this._polynomial;
    }

    public com.luver.table.castor.Table getTable() {
        return this._table;
    }

    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    public void marshal(final java.io.Writer out)
            throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        Marshaller.marshal(this, out);
    }

    public void marshal(final org.xml.sax.ContentHandler handler)
            throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        Marshaller.marshal(this, handler);
    }

    public void setPolynomial(final com.luver.polynomial.castor.Polynomial polynomial) {
        this._polynomial = polynomial;
    }

    public void setTable(final com.luver.table.castor.Table table) {
        this._table = table;
    }

    public static Record unmarshal(final java.io.Reader reader)
            throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (Record) Unmarshaller.unmarshal(Record.class, reader);
    }

    public void validate()
            throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}
