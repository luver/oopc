package com.luver;

import com.luver.polynomial.Polynomial;
import com.luver.table.Table;
import com.luver.xml.XMLUtil;

import java.io.IOException;
import java.util.Scanner;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class ConsoleClient implements Client {
    private Polynomial polynomial;
    private Table table;

    public void run() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Do you want to open file? y/n");
        if (scanner.next().equals("y")) {
            openRecord();
            printTable();
            printPolynom();
        } else {
            readRecord();
            printTable();
            printPolynom();
            saveRecord();
        }

        System.out.println("Choose interpolation (lin/lag): ");
        Function interpolation;
        if (scanner.next().equals("lag")) {
            interpolation = new LagrangeInterpolation(table);
        } else {
            interpolation = new LinearInterpolation(table);
        }

        Equation equation = new Equation(polynomial, interpolation);
        System.out.println("Enter bounds and error (order: left, right, error).");
        System.out.println("Example: " + table.getPoint(0).getX() + ", "
                + table.getPoint(table.getSize() - 1).getX() + ", 0.01");
        double leftBound = Double.parseDouble(scanner.next());
        double rightBound = Double.parseDouble(scanner.next());
        double error = Double.parseDouble(scanner.next());
        Solver solver = new DichotomySolver();
        double result = solver.solve(equation, leftBound, rightBound, error);
        System.out.print("Root: " + result);
        ReportBuilder reportBuilder = new ReportBuilder();
        reportBuilder.setTable(table);
        reportBuilder.setResult(result);
        reportBuilder.setPolynomial(polynomial);
        reportBuilder.setConstraints(leftBound, rightBound, error);
        reportBuilder.setInterpolation(interpolation);
        try {
            reportBuilder.createReport().saveToFile("/home/luver/report/");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void saveRecord() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Do you want to save it? y/n: ");
        if (scanner.next().equals("y")) {
            System.out.println("File name: ");
            XMLUtil.save(scanner.next(), table, polynomial);
        }
    }

    public void readTable() {
        Scanner scanner = new Scanner(System.in);
        String temp[];
        table = new Table();
        System.out.println("Let's create tables. Type 'x;y' without brackets please and '-' to stop");
        String command = scanner.nextLine();
        while (!command.equals("-")) {
            temp = command.split(";");
            table.addPoint(Double.parseDouble(temp[0]), Double.parseDouble(temp[1]));
            command = scanner.nextLine();
        }
    }


    public void readPolynom() {
        String command;
        polynomial = new Polynomial();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Let's create polynomial. Type 'x' without brackets and '-' to stop");
        int coefNumber = 0;
        System.out.print(coefNumber + ": ");
        command = scanner.nextLine();
        while (!command.equals("-")) {
            polynomial.setCoef(coefNumber, Double.parseDouble(command));
            coefNumber++;
            System.out.print(coefNumber + ": ");
            command = scanner.next();
        }
    }


    public void readRecord() {
        readTable();
        readPolynom();
    }

    private void printTable() {
        System.out.println("Here is your table:");
        for (int i = 0; i < table.getSize(); i++) {
            System.out.println(table.getPoint(i).getX() + "; " + table.getPoint(i).getY());
        }
    }

    private void printPolynom() {
        System.out.println("Here is your polynomial:");
        for (int i = 0; i < polynomial.getDegree(); i++) {
            System.out.println(polynomial.getCoef(i));
        }
    }

    private void openRecord() {
        System.out.println("File name: ");
        String command = (new Scanner(System.in)).next();
        //RecordBuilder builder = new RecordBuilder(command);
        table = XMLUtil.readTable(command);
        polynomial = XMLUtil.readPolynomial(command);
    }
}