package com.luver.table.castor;

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;


/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */


public class Point implements java.io.Serializable {
    private float _x;
    private boolean _has_x;
    private float _y;

    private boolean _has_y;

    public Point() {
        super();
    }

    public void deleteX() {
        this._has_x = false;
    }

    public void deleteY() {
        this._has_y = false;
    }

    public float getX() {
        return this._x;
    }

    public float getY() {
        return this._y;
    }

    public boolean hasX() {
        return this._has_x;
    }

    public boolean hasY() {
        return this._has_y;
    }

    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    public void marshal(final java.io.Writer out)
            throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        Marshaller.marshal(this, out);
    }

    public void marshal(final org.xml.sax.ContentHandler handler)
            throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        Marshaller.marshal(this, handler);
    }

    public void setX(final float x) {
        this._x = x;
        this._has_x = true;
    }

    public void setY(final float y) {
        this._y = y;
        this._has_y = true;
    }

    public static com.luver.table.castor.Point unmarshal(
            final java.io.Reader reader)
            throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (com.luver.table.castor.Point) Unmarshaller.unmarshal(com.luver.table.castor.Point.class, reader);
    }

    public void validate()
            throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}
