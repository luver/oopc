package com.luver.table.castor;

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */

public class Table implements java.io.Serializable {

    private java.util.Vector _pointList;

    public Table() {
        super();
        this._pointList = new java.util.Vector();
    }

    public void addPoint(final com.luver.table.castor.Point vPoint)
            throws java.lang.IndexOutOfBoundsException {
        this._pointList.addElement(vPoint);
    }

    public void addPoint(final int index,
                         final com.luver.table.castor.Point vPoint)
            throws java.lang.IndexOutOfBoundsException {
        this._pointList.add(index, vPoint);
    }

    public java.util.Enumeration enumeratePoint() {
        return this._pointList.elements();
    }

    public com.luver.table.castor.Point getPoint(final int index)
            throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._pointList.size()) {
            throw new IndexOutOfBoundsException("getPoint: Index value '" + index + "' not in range [0.." + (this._pointList.size() - 1) + "]");
        }

        return (com.luver.table.castor.Point) _pointList.get(index);
    }

    public com.luver.table.castor.Point[] getPoint() {
        com.luver.table.castor.Point[] array = new com.luver.table.castor.Point[0];
        return (com.luver.table.castor.Point[]) this._pointList.toArray(array);
    }

    public int getPointCount() {
        return this._pointList.size();
    }

    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }


    public void marshal(final java.io.Writer out)
            throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        Marshaller.marshal(this, out);
    }

    public void marshal(final org.xml.sax.ContentHandler handler)
            throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        Marshaller.marshal(this, handler);
    }

    public void removeAllPoint() {
        this._pointList.clear();
    }

    public boolean removePoint(final com.luver.table.castor.Point vPoint) {
        return _pointList.remove(vPoint);
    }

    public com.luver.table.castor.Point removePointAt(final int index) {
        java.lang.Object obj = this._pointList.remove(index);
        return (com.luver.table.castor.Point) obj;
    }

    public void setPoint(final int index,
                         final com.luver.table.castor.Point vPoint)
            throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._pointList.size()) {
            throw new IndexOutOfBoundsException("setPoint: Index value '" + index + "' not in range [0.." + (this._pointList.size() - 1) + "]");
        }

        this._pointList.set(index, vPoint);
    }


    public void setPoint(final com.luver.table.castor.Point[] vPointArray) {
        //-- copy array
        _pointList.clear();

        for (int i = 0; i < vPointArray.length; i++) {
            this._pointList.add(vPointArray[i]);
        }
    }

    public static com.luver.table.castor.Table unmarshal(final java.io.Reader reader)
            throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (com.luver.table.castor.Table) Unmarshaller.unmarshal(com.luver.table.castor.Table.class, reader);
    }

    public void validate()
            throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}