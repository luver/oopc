package com.luver.swingClient.models;

import com.luver.table.Point;
import com.luver.table.Table;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.AbstractTableModel;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class TableJTableModel extends AbstractTableModel {
    TableModelListener listener;

    @Override
    public void addTableModelListener(TableModelListener listener) {
        this.listener = listener;
    }

    private Table table = new Table();
    private int count;

    public TableJTableModel() {
    }

    public TableJTableModel(Table table) {
        this.table = table;
        count = table.getSize();
    }

    public Table getTable() {
        return table;
    }

    @Override
    public int getRowCount() {
        return table.getSize();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (rowIndex > count) throw new RuntimeException();
        Point point = table.getPoint(rowIndex);
        if (columnIndex == 0) return point.getX();
        if (columnIndex == 1) return point.getY();
        return null;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return true;
    }

    @Override
    public String getColumnName(int index) {
        return (index == 0) ? "x" : "y";
    }

    public void setValueAt(Object value, int rowIndex, int columnIndex) {
        if (rowIndex > count) throw new RuntimeException();
        if (columnIndex == 0) {
            table.setPoint(rowIndex, new Point(Double.parseDouble(value.toString()),
                    (Double) getValueAt(rowIndex, columnIndex + 1)));
        } else {
            table.setPoint(rowIndex,
                    new Point((Double) getValueAt(rowIndex, columnIndex - 1), Double.parseDouble(value.toString())));
        }
        listener.tableChanged(new TableModelEvent(this));
    }

    public void addPoint() {
        table.addPoint(0, 0);
        count = table.getSize();
    }

    public void deletePoint() {
        table.deletePoint();
    }
}
