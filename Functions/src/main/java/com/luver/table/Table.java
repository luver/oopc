package com.luver.table;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class Table {
    private ArrayList<Point> points = new ArrayList<Point>();

    public void addPoint(double x, double y) {
        points.add(new Point(x, y));
    }

    public void setPoint(int x, Point point) {
        points.set(x, point);
    }

    public int getSize() {
        return points.size();
    }

    public Point getPoint(int number) {
        if (number >= points.size()) {
            throw new IllegalArgumentException("Невiрно задана таблиця");
        }
        return points.get(number);
    }

    public void sortTable() {
        Collections.sort(points, new Comparator<Point>() {
            @Override
            public int compare(Point point, Point point1) {
                if (point.getX() > point1.getX()) return 1;
                if (point.getX() < point1.getX()) return -1;
                return 0;
            }
        });
    }

    public void deletePoint() {
        points.remove(points.size() - 1);
    }
}
