package com.luver;

import com.luver.polynomial.Polynomial;
import com.luver.table.Table;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;

import java.util.ArrayList;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class ReportBuilder {
    private Report report = new Report();

    private Table table;
    private Polynomial polynomial;
    private Function interpolation;

    public Report createReport() {
        createChart();
        return report;
    }

    public void setPolynomial(Polynomial polynomial) {
        this.polynomial = polynomial;
        ArrayList<double[]> raw = new ArrayList<double[]>();
        for (int i = 0; i < polynomial.getDegree(); i++) {
            raw.add(new double[]{i, polynomial.getCoef(i)});
        }
        report.setPolynom(raw);
    }

    public void setTable(Table table) {
        this.table = table;
        ArrayList<double[]> raw = new ArrayList<double[]>();
        for (int i = 0; i < table.getSize(); i++) {
            raw.add(new double[]{table.getPoint(i).getX(), table.getPoint(i).getY()});
        }
        report.setTable(raw);
    }

    public void setConstraints(double left, double right, double error) {
        report.setConstraints(left, right, error);
    }

    public void setResult(double result) {
        report.setResult(result);
    }

    public void setInterpolation(Function interpolation) {
        this.interpolation = interpolation;

    }

    private void createChart() {
        XYSeries polynomialSeries = new XYSeries("Полiном", false, true);

        XYSeries tableSeries = new XYSeries("Таблиця", false, true);
        XYSeries interpolationSeries = new XYSeries("Iнтерпольована функцiя", false, true);
        double min = table.getPoint(0).getX();
        double max = table.getPoint(table.getSize() - 1).getX();

        for (double i = min; i < max; i += 0.01) {
            double temp = polynomial.eval(i);
            polynomialSeries.add(temp, i);
            temp =  interpolation.eval(i);
            interpolationSeries.add(temp, i);
            System.out.println("("+i+"; "+temp+")");
        }

        XYSeriesCollection xyDataset = new XYSeriesCollection();
        xyDataset.addSeries(interpolationSeries);
        xyDataset.addSeries(polynomialSeries);

        JFreeChart chart = ChartFactory.createXYLineChart("Графiк", "Y", "X",
                xyDataset, PlotOrientation.HORIZONTAL, true, false, false);
        report.setChart(chart);
    }
}
