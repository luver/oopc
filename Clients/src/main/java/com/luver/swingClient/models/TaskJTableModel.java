package com.luver.swingClient.models;

import javax.swing.table.AbstractTableModel;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class TaskJTableModel extends AbstractTableModel {
    private double left;
    private double right;
    private double error;

    @Override
    public int getRowCount() {
        return 3;
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (columnIndex == 0) {
            if (rowIndex == 0) return "X вiд";
            if (rowIndex == 1) return "X до";
            if (rowIndex == 2) return "Похибка";
        } else {
            if (rowIndex == 0) return left;
            if (rowIndex == 1) return right;
            if (rowIndex == 2) return error;
        }
        return null;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return columnIndex != 0;
    }

    @Override
    public void setValueAt(Object value, int rowIndex, int columnIndex) {
        if (columnIndex == 1) {
            if (rowIndex == 0) left = Double.parseDouble(value.toString());
            if (rowIndex == 1) right = Double.parseDouble(value.toString());
            if (rowIndex == 2) error = Double.parseDouble(value.toString());
        }
    }
}
