package com.luver.polynomial.castor;

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */


public class Polynomial implements java.io.Serializable {

    private java.util.Vector _coefList;

    public Polynomial() {
        super();
        this._coefList = new java.util.Vector();
    }

    public void addCoef(final Coef vCoef)
            throws java.lang.IndexOutOfBoundsException {
        this._coefList.addElement(vCoef);
    }

    public void addCoef(
            final int index,
            final Coef vCoef)
            throws java.lang.IndexOutOfBoundsException {
        this._coefList.add(index, vCoef);
    }

    public java.util.Enumeration enumerateCoef(
    ) {
        return this._coefList.elements();
    }

    public Coef getCoef(final int index)
            throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._coefList.size()) {
            throw new IndexOutOfBoundsException("getCoef: Index value '" + index + "' not in range [0.." + (this._coefList.size() - 1) + "]");
        }

        return (Coef) _coefList.get(index);
    }

    public Coef[] getCoef() {
        Coef[] array = new Coef[0];
        return (Coef[]) this._coefList.toArray(array);
    }

    public int getCoefCount() {
        return this._coefList.size();
    }

    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    public void marshal(final java.io.Writer out)
            throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        Marshaller.marshal(this, out);
    }

    public void marshal(final org.xml.sax.ContentHandler handler)
            throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        Marshaller.marshal(this, handler);
    }


    public void removeAllCoef() {
        this._coefList.clear();
    }


    public boolean removeCoef(final Coef vCoef) {
        return _coefList.remove(vCoef);
    }

    public Coef removeCoefAt(final int index) {
        java.lang.Object obj = this._coefList.remove(index);
        return (Coef) obj;
    }


    public void setCoef(final int index,
                        final Coef vCoef)
            throws java.lang.IndexOutOfBoundsException {
        // check bounds for index
        if (index < 0 || index >= this._coefList.size()) {
            throw new IndexOutOfBoundsException("addCoef: Index value '" + index + "' not in range [0.." + (this._coefList.size() - 1) + "]");
        }

        this._coefList.set(index, vCoef);
    }


    public void setCoef(final Coef[] vCoefArray) {
        //-- copy array
        _coefList.clear();

        for (int i = 0; i < vCoefArray.length; i++) {
            this._coefList.add(vCoefArray[i]);
        }
    }


    public static Polynomial unmarshal(final java.io.Reader reader)
            throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (Polynomial) Unmarshaller.unmarshal(Polynomial.class, reader);
    }


    public void validate()
            throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}