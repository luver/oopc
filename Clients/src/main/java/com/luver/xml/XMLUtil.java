package com.luver.xml;

import com.luver.polynomial.Polynomial;
import com.luver.polynomial.castor.Coef;
import com.luver.table.Point;
import com.luver.table.Table;

import java.io.FileReader;
import java.io.FileWriter;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class XMLUtil {
    static public void save(String file, Table table, Polynomial polynomial) {
        com.luver.table.castor.Table xmlTable = new com.luver.table.castor.Table();
        Point tempPoint;
        com.luver.table.castor.Point tempCastorPoint;
        for (int i = 0; i < table.getSize(); i++) {
            tempPoint = table.getPoint(i);
            tempCastorPoint = new com.luver.table.castor.Point();
            tempCastorPoint.setX((float) tempPoint.getX());
            tempCastorPoint.setY((float) tempPoint.getY());

            xmlTable.addPoint(tempCastorPoint);
        }

        com.luver.polynomial.castor.Polynomial castorPolynomial = new com.luver.polynomial.castor.Polynomial();
        com.luver.polynomial.castor.Coef tempCoef;

        for (int i = 0; i < polynomial.getDegree(); i++) {
            tempCoef = new Coef();
            tempCoef.setNumber(i);
            tempCoef.setValue((float) polynomial.getCoef(i));
            castorPolynomial.addCoef(tempCoef);
        }

        com.luver.xml.Record record = new Record();
        record.setPolynomial(castorPolynomial);
        record.setTable(xmlTable);
        try {
            record.marshal(new FileWriter(file));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static public Table readTable(String filename) {
        try {
            FileReader reader = new FileReader(filename);
            Record record = Record.unmarshal(reader);
            com.luver.table.castor.Table xmlTable = record.getTable();
            Table table = new Table();
            for (com.luver.table.castor.Point xmlPoint : xmlTable.getPoint()) {
                table.addPoint(xmlPoint.getX(), xmlPoint.getY());
            }
            return table;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    static public Polynomial readPolynomial(String filename) {
        try {
            FileReader reader = new FileReader(filename);
            Record record = Record.unmarshal(reader);
            com.luver.polynomial.castor.Polynomial xmlPolynomial = record.getPolynomial();
            Polynomial polynomial = new Polynomial();
            for (Coef xmlCoef : xmlPolynomial.getCoef()) {
                polynomial.setCoef(xmlCoef.getNumber(), xmlCoef.getValue());
            }
            return polynomial;

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
