package com.luver.polynomial;

import com.luver.Function;

import java.util.HashMap;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class Polynomial implements Function {
    private HashMap<Integer, Double> coefs = new HashMap<Integer, Double>();

    @Override
    public double eval(double x) {
        double temp = 0;
        for (int i = 0; i < coefs.size(); i++) {
            temp += Math.pow(x, i) * coefs.get(i);
        }
        return temp;
    }

    public void setCoef(int number, double value) {
        if (number < 0) {
            throw new IllegalArgumentException("Помилка при заданнi полiнома.");
        }
        coefs.put(number, value);
    }

    public double getCoef(int number) {
        if (number < 0) {
            throw new IllegalArgumentException("Неправильний номер коеф.");
        }
        return coefs.get(number);
    }

    public void addCoef(double coef) {
        coefs.put(getDegree(), coef);
    }

    public int getDegree() {
        return coefs.size();
    }

    public void deleteCoef() {
        coefs.remove(coefs.size() - 1);
    }

}