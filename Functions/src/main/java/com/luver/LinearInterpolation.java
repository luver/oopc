package com.luver;

import com.luver.table.Table;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class LinearInterpolation implements Function {
    private final Table table;

    public LinearInterpolation(Table table) {
        this.table = table;

    }

    @Override
    public double eval(double x) {
        table.sortTable();
        if (!isInBounds(x)) {
            throw new IllegalArgumentException("X поза iнтервалом");
        }
        int leftIndex = 0, rightIndex = 1;
        while (!(x >= table.getPoint(leftIndex).getX() && x <= table.getPoint(rightIndex).getX())) {
            if (table.getSize() > rightIndex + 1) {
                leftIndex++;
                rightIndex++;
            } else break;
        }

        return table.getPoint(leftIndex).getY() +
                ((x - table.getPoint(leftIndex).getX()) *
                        ((table.getPoint(rightIndex).getY() - table.getPoint(leftIndex).getY()) /
                                (table.getPoint(rightIndex).getX() - table.getPoint(leftIndex).getX())));
    }

    private boolean isInBounds(double x) {
        return (x >= table.getPoint(0).getX() && x <= table.getPoint(table.getSize() - 1).getX());
    }
}
