package com.luver;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public interface Solver {
    double solve(Equation equation, double left, double right, double error);
}
