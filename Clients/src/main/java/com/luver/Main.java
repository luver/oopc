package com.luver;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class Main {
    public static void main(String[] args) {
        Client client;
        if (args.length > 0) {
            client = new ConsoleClient();
        } else {
            client = new SwingClient();
        }
        client.run();
    }
}
