package com.luver.polynomial.castor;

import org.exolab.castor.xml.Marshaller;
import org.exolab.castor.xml.Unmarshaller;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */

public class Coef implements java.io.Serializable {
    private int _number;
    private boolean _has_number;
    private float _value;
    private boolean _has_value;

    public Coef() {
        super();
    }

    public void deleteNumber() {
        this._has_number = false;
    }

    public void deleteValue() {
        this._has_value = false;
    }

    public int getNumber() {
        return this._number;
    }

    public float getValue() {
        return this._value;
    }

    public boolean hasNumber() {
        return this._has_number;
    }

    public boolean hasValue() {
        return this._has_value;
    }

    public boolean isValid() {
        try {
            validate();
        } catch (org.exolab.castor.xml.ValidationException vex) {
            return false;
        }
        return true;
    }

    public void marshal(final java.io.Writer out)
            throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        Marshaller.marshal(this, out);
    }

    public void marshal(final org.xml.sax.ContentHandler handler)
            throws java.io.IOException, org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        Marshaller.marshal(this, handler);
    }

    public void setNumber(final int number) {
        this._number = number;
        this._has_number = true;
    }

    public void setValue(final float value) {
        this._value = value;
        this._has_value = true;
    }

    public static Coef unmarshal(final java.io.Reader reader)
            throws org.exolab.castor.xml.MarshalException, org.exolab.castor.xml.ValidationException {
        return (Coef) Unmarshaller.unmarshal(Coef.class, reader);
    }

    public void validate()
            throws org.exolab.castor.xml.ValidationException {
        org.exolab.castor.xml.Validator validator = new org.exolab.castor.xml.Validator();
        validator.validate(this);
    }

}