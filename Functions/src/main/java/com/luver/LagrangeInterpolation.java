package com.luver;

import com.luver.table.Table;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class LagrangeInterpolation implements Function {
    private final Table table;

    public LagrangeInterpolation(Table table) {
        this.table = table;

    }

    @Override
    public double eval(double x) {
        table.sortTable();
        if (!isInBounds(x)) {
            throw new IllegalArgumentException("X поза iнтервалом");
        }

        double production;
        double result = 0, iterationResult;
        for (int k = 0; k < table.getSize(); k++) {

            production = 1;
            iterationResult = 1;
            iterationResult *= table.getPoint(k).getY();
            for (int j = 0; j < table.getSize(); j++) {
                if (j != k)
                    production *= (x - table.getPoint(j).getX()) / (table.getPoint(k).getX() - table.getPoint(j).getX());
            }
            iterationResult *= production;
            result += iterationResult;
        }
        return result;
    }

    private boolean isInBounds(double x) {
        return (x >= table.getPoint(0).getX() && x <= table.getPoint(table.getSize() - 1).getX());
    }
}
