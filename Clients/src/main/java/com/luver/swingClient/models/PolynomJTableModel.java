package com.luver.swingClient.models;

import com.luver.polynomial.Polynomial;

import javax.swing.table.AbstractTableModel;

/**
 * Author: Vladislav Lubenskiy, vlad.lubenskiy@gmail.com
 */
public class PolynomJTableModel extends AbstractTableModel {
    private Polynomial polynomial;

    public PolynomJTableModel() {
        polynomial = new Polynomial();
    }

    public PolynomJTableModel(Polynomial polynomial) {
        this.polynomial = polynomial;
    }

    public Polynomial getPolynomial() {
        return polynomial;
    }

    @Override
    public int getRowCount() {
        return polynomial.getDegree();
    }

    @Override
    public int getColumnCount() {
        return 2;
    }

    public void addCoef() {
        polynomial.addCoef(0.0);
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        if (columnIndex == 0) return rowIndex + 1;
        if (polynomial.getDegree() >= rowIndex + 1) {
            return polynomial.getCoef(rowIndex);
        } else return 0;
    }

    public void setValueAt(Object value, int rowIndex, int columnIndex) {
        polynomial.setCoef(rowIndex, Double.parseDouble(value.toString()));
        fireTableCellUpdated(rowIndex, columnIndex);
    }

    @Override
    public String getColumnName(int index) {
        return (index == 0) ? "x" : "y";
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return columnIndex != 0;
    }

    public void deleteCoef() {
        polynomial.deleteCoef();
    }
}
